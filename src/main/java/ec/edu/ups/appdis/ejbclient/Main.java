package ec.edu.ups.appdis.ejbclient;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.jboss.tools.examples.model.Member;
import org.jboss.tools.examples.service.MemberRegistrationRemote;

public class Main {
	
	private MemberRegistrationRemote memberRegistration;
	
	public void intanciarMemberRegistration() throws Exception {
		try {  
            final Hashtable<String, Comparable> jndiProperties =  
                    new Hashtable<String, Comparable>();  
            jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY,  
                    "org.wildfly.naming.client.WildFlyInitialContextFactory");  
            jndiProperties.put("jboss.naming.client.ejb.context", true);  
              
            jndiProperties.put(Context.PROVIDER_URL, "http-remoting://localhost:8080");  
            jndiProperties.put(Context.SECURITY_PRINCIPAL, "ejb");  
            jndiProperties.put(Context.SECURITY_CREDENTIALS, "xxxxxx_xx");  
              
            final Context context = new InitialContext(jndiProperties);  
              
            final String lookupName = "ejb:/jboss-javaee-webapp/MemberRegistration!org.jboss.tools.examples.service.MemberRegistrationRemote";  
              
            this.memberRegistration = (MemberRegistrationRemote) context.lookup(lookupName);  
              
        } catch (Exception ex) {  
            ex.printStackTrace();  
            throw ex;  
        }  
	}
	
	public void registrar(String email, String name, String phone) throws Exception {
		Member m = new Member();
		m.setEmail(email);
		m.setName(name);
		m.setPhoneNumber(phone);
		
		memberRegistration.register(m);
	}

	public static void main(String[] args) {	
		Main main = new Main();
		try {
			main.intanciarMemberRegistration();
			main.registrar("ctimbi@hotmail.com", "Cristian TS", "0984864934");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
